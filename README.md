# tram_schedule_fourletter_rpi_py

This bot uses https://api-ratp.pierre-grimaud.fr/v4 API to get the next 2 arrivals time of the T2 tram on the Meudon-sur-Seine tram stop.

It shows the result on a (bad-welded) four-letter module for my raspberry pi 3.

It is my first python programme.