import requests
import fourletterphat as flp
import time
def getArrivalTime(timeTxt):
	time = ""
	firstChar = timeTxt[0]
	if firstChar.isnumeric():
		secondChar = timeTxt[1]
		if secondChar.isnumeric():
			time = firstChar + secondChar
		else:
			time = "0" + firstChar
	else:
		time = "00"
	return time
URL = "https://api-ratp.pierre-grimaud.fr/v4/schedules/tramways/2/meudon+sur+seine/R"
while True:
	flp.clear()
	sleepDuration = 60
	data = requests.get(url = URL).json()
	try:
		time1Txt = data['result']['schedules'][0]['message']
		time2Txt = data['result']['schedules'][1]['message']
	except:
		timeTxt = "EROR"
		sleepDuration *= 10
	else:
		time1 = getArrivalTime(time1Txt)
		time2 = getArrivalTime(time2Txt)
		timeTxt = "%s.%s"%(time1,time2)
	flp.print_number_str(timeTxt)
	flp.show()
	time.sleep(sleepDuration)